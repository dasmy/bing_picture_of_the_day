#!/usr/bin/env python

import os
from subprocess import call
from time import sleep
from urllib.request import urlopen, urlretrieve
from xml.dom.minidom import parseString
from datetime import datetime, date


# Defines source and destination of image
host = 'https://www.bing.com/'
xml_link = host + 'HPImageArchive.aspx?idx=0&n=1&mkt=en-WW'
dst_dir = os.path.expanduser('~/Pictures/DeskFeed/')

SCRIPT = 'tell application "System Events" to set picture of current desktop to "{}"'
TIMEOUT = 360


def set_desktop_background(destination):
    message(f'Setting desktop background to {destination}')
    return asrun(SCRIPT.format(destination))


# http://www.leancrew.com/all-this/2013/03/combining-python-and-applescript/
def asrun(ascript):
    "Run the given AppleScript and return the standard output and error."
    return call(['osascript', '-e', ascript]) == 0


def message(msg):
    print(f'{datetime.now():%Y-%m-%d %H:%M:%S} : {msg}')


def parse_xml(link):
    try:
        xml_contents = urlopen(link)
        xml = xml_contents.read()
        xml_contents.close()

        dom = parseString(xml)
        firstitem = dom.getElementsByTagName('url')[0]
        rel_link = firstitem.childNodes[0].data
        # increase resolution
        rel_link = rel_link.replace('1366x768', '1920x1080')

        return host + rel_link
    except:
        message(f'Failed to read xml file {link}')
        return None


def main():
    while True:
        destination = f'{dst_dir}{date.today():%y-%m-%d}.jpg'

        while not os.path.exists(destination):
            image_url = parse_xml(xml_link)

            if image_url:
                message(f'Downloading\n\t{image_url}\nto\n\t{destination}')
                urlretrieve(image_url, destination)
            else:
                sleep(TIMEOUT)

        if set_desktop_background(destination):
            return

        # for the sake of "Required bahviours" in https://developer.apple.com/library/mac/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/CreatingLaunchdJobs.html :
        # You must not daemonize your process. This includes calling the daemon function, calling fork followed by exec, or calling fork followed by exit. If you do, launchd thinks your process has died. Depending on your property list key settings, launchd will either keep trying to relaunch your process until it gives up (with a "respawning too fast" error message) or will be unable to restart it if it really does die.
        message(f'Sleeping for {TIMEOUT:%d} seconds....')
        sleep(TIMEOUT)


if __name__ == '__main__':
    main()
