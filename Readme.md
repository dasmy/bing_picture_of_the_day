Launchd script for automatically setting the Bing Picture of the day as OSX wallpaper.

# About
This is a small tool that automatically sets the OSX Desktop background on the primary screen to the current [Bing picture of the day](http://www.bing.com/gallery/).
For doing so it fetches the picture every day at 10am, puts it into `${HOME}/Pictures/DeskFeed` and installs it as background image.
If there is no internet connection at 10am, the script continually retries until it works :-)

# Setup
The script needs some adaptions w.r.t. the user name and has to reside in `${HOME}/Pictures/DeskFeed`.

    cd ${HOME}/Pictures

Download the scripts

    git clone https://bitbucket.org/dasmy/Bing_Picture_Of_The_Day ./DeskFeed
    cd DeskFeed

Replace mathias in bing.plist with your username:

    sed -i '' 's/mathias/USERNAME/g' bing.plist

Install the launchd task:

    launchctl load -w bing.plist

For automatic execution upon login, put a symlink to bing.plist into your `${HOME}/Library/LaunchAgents`:

    mkdir -p ${HOME}/Library/LaunchAgents
    cd ${HOME}/Library/LaunchAgents
    ln -s ${HOME}/Pictures/DeskFeed/bing.plist
